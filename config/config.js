//puerto
process.env.PORT = process.env.PORT || 3000

//entorno
process.env.NODE_ENV = process.env.NODE_ENV || 'desv'

//token
//vencimiento del token
process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30
//SEED de autenticacion
process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo'
//google
process.env.CLIENT_ID = process.env.CLIENT_ID || '757399235295-fj6oirmiojncf3140e01fbhqv410hmfo.apps.googleusercontent.com' 
                                                
//base de datos
let urlDB;
if(process.env.NODE_ENV === 'dev'){
    urlDB = 'mongodb://localhost:27017/lab07DB'
} else {
    urlDB = 'mongodb+srv://alvaromedina:XXREVOLUTIONXX12@cluster0-xbwmn.mongodb.net/lab07DB'
}
process.env.URLDB = urlDB
