const express = require('express');
const mongoose = require('mongoose');
const app = express();
const routesU = require('./routes/routesUser');
const routesN = require('./routes/routesArticulos');
const methodOverride = require('method-override');
require('./config/config');
const cookieParser = require('cookie-parser');// librería para configurar cookies
const bodyParser = require('body-parser');
const session = require('express-session'); // librería de autentifiación
const fileUpload = require('express-fileupload');
const bodyParserJSON = bodyParser.json();
const bodyParserURLEncoded = bodyParser.urlencoded({ extended: true });
app.use(bodyParserJSON);
app.use(bodyParserURLEncoded);
app.use(cookieParser()); // Inicializacion de la librería
app.use(fileUpload({ useTempFiles:true }));

mongoose.connect(process.env.URLDB, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false
}, (err, des) =>{
  if(err) throw err;
  console.log('Base de datos online')
});

app.use(express.static(__dirname + '/public'));
app.set('view engine', 'pug');
app.use(methodOverride('_method'));

// Inicializar el metodo de auntentifiación
app.use(session({
  secret: 'keyboard-cat',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true, httpOnly: true } // Adicional para el trbajo con cookies
}))
app.use((req, res, next)=>{
  res.locals.user = req.cookies.token || null;
  next();
});

app.listen(process.env.PORT, () => console.log(`Escuchando puerto ${process.env.PORT}`));

app.use(routesU);
app.use(routesN);

app.use(require('./routes/upload'))


app.use(function(req, res, next) {
    res.status(404).sendFile(process.cwd() + '/app/views/404');
});