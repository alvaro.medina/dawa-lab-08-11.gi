const jwt = require('jsonwebtoken');

let verificaToken = (req, res, next) => {
    let token = req.cookies.token;
    jwt.verify(token, process.env.SEED, (err, decoded)=>{
        if(err) {
            console.log(err)
            res.clearCookie("token");
            res.redirect("/login");
        }
        req.user = decoded.usuario;
        next();
    });
};

module.exports = {
    verificaToken,
};