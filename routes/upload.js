const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

const {verificaToken} = require('../middleware/authetication');
const Usuario = require('../models/usuario');

const fs = require('fs');
const path = require('path');

app.get('/perfil', verificaToken, async function (req, res) {
    let id = req.user._id
    let user = await Usuario.findById(id)
    res.render('profile', {
        user
    })
});

app.put('/updatePerfil/:id', verificaToken, async function(req, res){
    let id = req.params.id;
    let img = req.files.avatar;
    let { nombre, email, nameImg } = req.body;
    let nombreArchivoCortado = img.name.split('.');
    let extencion = nombreArchivoCortado[nombreArchivoCortado.length - 1];
    let extencionesValidas = ["png", "jpg", "jpeg"];
    if (extencionesValidas.indexOf(extencion) < 0) {
        res.redirect("/perfil");
    } else {
        let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extencion}`
        img.mv(`public/assets/img/${nombreArchivo}`, (err) => {
            if (err) {
                res.redirect("/perfil");
            }
        });
        let pathImage = path.resolve(__dirname, `../public/assets/img/${nameImg}`);
        console.log(pathImage)
        if(fs.existsSync(pathImage)){
            fs.unlinkSync(pathImage)
        }
        await Usuario.findByIdAndUpdate(req.params.id, { nombre: nombre, email: email, img: nombreArchivo });
        res.redirect("/perfil");
    }
});

module.exports = app;