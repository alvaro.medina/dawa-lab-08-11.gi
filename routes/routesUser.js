const express = require('express');
const bcrypt = require('bcrypt');
const Usuario = require('../models/usuario');
const {verificaToken} = require('../middleware/authetication');
const router = express.Router();
const jwt = require('jsonwebtoken');
const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);

router.get('/', function(req, res){
    res.render('home')
});
router.get('/login', function(req, res){
    let error = [];
    res.render('login', {error, 
        username: '',
        email: '',
        password: '',
        confirmPassword: ''})
});
router.get('/register', function(req, res){
    let error = [];
    res.render('register', {error, 
        username: '',
        email: '',
        password: '',
        confirmPassword: ''})
});
router.post('/registrar',  async function(req, res) {
    let error = [];
    const { username, email, password, confirmPassword } = req.body;
    if (password != confirmPassword) {
        error.push({ text: "Contraseñas no coinciden." });
        res.render("register", {
            error,
            username,
            email,
            password,
            confirmPassword
        });
    } else {
        const emailUser = await Usuario.findOne({ email: email });
        const nameUser = await Usuario.findOne({ nombre: username });
        if (emailUser) {
            error.push({ text: "El correo ya esta en uso." });
        }
        if (nameUser) {
            error.push({ text: "El usuario ya esta en uso." });
        }
        if(error.length >0 ){
            res.render("register", {
                error,
                username,
                email,
                password,
                confirmPassword
            });
        } else {
          // Saving a New User
          const newUser = new Usuario({ nombre:username, email:email, password:password, img:'avatar.png' });
          newUser.password = await newUser.encryptPassword(password);
          await newUser.save();
          let token = jwt.sign({
            usuario: newUser
          }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN })
          res.cookie('token', token)
          res.redirect("/articulos");
        }
    }
    
});

router.post('/ingresar', async function(req, res) {
    let error = [];
    const { email, password } = req.body;
    const user = await Usuario.findOne({email: email});
    if (!user) {
        error.push({ text: "Credenciales inválidas." });
        res.render("login", {
            error,
            email,
            password
        });
    } else {
        const match = await user.matchPassword(password);
        if(match) {
            let token = jwt.sign({
                usuario: user
            }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN })
            res.cookie('token', token)
            res.redirect('/articulos')
        } else {
            error.push({ text: "Credenciales inválidas." });
            res.render("login", {
                error,
                email,
                password
            });
        }
        
    }
});

//Uso de cookie para cerar sesion => Sesion Normal y Sesion de Google
router.get('/salir', (req, res) => {
    res.clearCookie("token");
    res.redirect("/");
});


//Google
async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID,
    });
    const payload = ticket.getPayload();
    return payload;
}

router.post("/google", async (req, res) => {
    let token = req.body.idtoken;
    let googleUser = await verify(token).catch((e)=>{
        return res.status(403).json({
            ok: false,
            err: e
        });
    });
    let error = [];
    Usuario.findOne({ email: googleUser.email }, async (err, usuarioDB)=>{
        if(err) {
            res.status(500).json({
                ok: false,
                err
            });
        }
        if(usuarioDB) {
            if (usuarioDB.google === false){
                error.push({ text: 'Debe de usar su autenticación normal' });
                res.render("login", {
                    error
                });
            } else {
                let token = jwt.sign(
                    { usuario: usuarioDB },
                    process.env.SEED,
                    { expiresIn: process.env.CADUCIDAD_TOKEN }
                );
                res.cookie('token', token)
                res.redirect('/articulos')
            }
        } else {
            let usuario = new Usuario();
            usuario.nombre = googleUser.name;
            usuario.email = googleUser.email;
            usuario.img = googleUser.picture;
            usuario.google = true;
            usuario.password = await usuario.encryptPassword('password');

            usuario.save((err, usuarioDB) => {
                if(err){
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }
                let token = jwt.sign(
                    { usuario: usuarioDB },
                    process.env.SEED,
                    { expiresIn: process.env.CADUCIDAD_TOKEN }
                );
                res.cookie('token', token)
                res.redirect('/articulos')
            });
        }
    });
});

module.exports = router